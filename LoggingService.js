'use strict';

const
  Service = require('kermit/Service'),
  bunyan = require('bunyan');

class Logging extends Service {
  /**
   * Bootstrap the logging service.
   *
   * @return {Logging}
   */
  bootstrap() {
    let
      bunyanConfig = this.serviceConfig.get('bunyan', {}),
      streamsConfig = bunyanConfig.streams;

    // streams have been configured as object hash
    if (streamsConfig && !Array.isArray(streamsConfig)) {
      let streams = [], stream;

      for (let streamKey in streamsConfig) {
        stream = streamsConfig[streamKey];

        if (!stream.name) {
          stream.name = streamKey;
        }

        streams.push(stream);
      }

      bunyanConfig.streams = streams;
    }

    this.logger = bunyan.createLogger(bunyanConfig);

    return this;
  }

  /**
   * Return the application logger instance.
   *
   * @returns {bunyan.Logger}
   */
  getLogger() {
    return this.logger;
  }

  /**
   * Create a child logger with component set to the given name.
   *
   * @param componentName
   * @returns {bunyan.Logger}
   */
  getComponentLogger(componentName) {
    return this.logger.child({
      component: componentName
    });
  }

  /**
   * Return the current logging level.
   *
   * @param {boolean} asInteger - Whether to return the log level as integer.
   * @returns {string|integer}
   */
  getLevel(asInteger) {
    let level = this.logger.level();

    if (asInteger === true) {
      return level;
    } else {
      return bunyan.nameFromLevel(level);
    }
  }

  /**
   * Set the log level of all streams.
   *
   * @param level
   * @returns {Logging}
   */
  setLevel(level) {
    this.logger.level(level);

    return this;
  }

  /**
   * Logging from external libraries used by your app or very detailed application logging.
   *
   * @returns {Logging}
   */
  trace() {
    this.logger.trace(...arguments);

    return this;
  }

  /**
   * Anything else, i.e. too verbose to be included in "info" level.
   *
   * @returns {Logging}
   */
  debug() {
    this.logger.debug(...arguments);

    return this;
  }

  /**
   * Detail on regular operation.
   *
   * @returns {Logging}
   */
  info() {
    this.logger.info(...arguments);

    return this;
  }

  /**
   * A note on something that should probably be looked at by an operator eventually.
   *
   * @returns {Logging}
   */
  warn() {
    this.logger.warn(...arguments);

    return this;
  }

  /**
   * Fatal for a particular request, but the service/app continues servicing other requests. An operator should look at this soon(ish).
   *
   * @returns {Logging}
   */
  error() {
    this.logger.error(...arguments);

    return this;
  }

  /**
   * The service/app is going to stop or become unusable now. An operator should definitely look into this soon.
   * 
   * @returns {Logging}
   */
  fatal() {
    this.logger.fatal(...arguments);

    return this;
  }
}

module.exports = Logging;
